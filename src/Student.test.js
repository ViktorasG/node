import Student from './Student'

test('Student.sayHello', () => {
  const studentJonas = new Student('Jonas')

  expect(studentJonas.sayHello()).toBe('Jonas')
})
